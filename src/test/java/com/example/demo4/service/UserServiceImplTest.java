package com.example.demo4.service;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.demo4.exception.UserNotFoundException;
import com.example.demo4.model.User;
import com.example.demo4.repository.UserRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl userServiceImpl;

	@Mock
	UserRepository userRepository;

	@Test
	public void testFindByNameForPositive() {
		User user = new User((long) 1, "jansi", "jansi@gmail.com", "jansi");
		Mockito.when(userRepository.findByUsername("jansi")).thenReturn(user);
		User user1 = userServiceImpl.readByUserName("jansi");
		Assert.assertNotNull(user1);
		Assert.assertEquals("jansi", user1.getUsername());
	}
	
	@Test
	public void testFindByIdForNegative() {
		User user = new User((long) -15, "jansi", "jansi@gmail.com", "jansi");
		Mockito.when(userRepository.findById((long) -15)).thenReturn(Optional.of(user));
		User user1 = userServiceImpl.getUserById(-15);
		Assert.assertNotNull(user1);
		Assert.assertEquals("jansi@gmail.com", user1.getEmail());
	}
	

	@Test
	public void testFindByIdForPositive() {
		User user = new User((long) 15, "jansi", "jansi@gmail.com", "jansi");
		Mockito.when(userRepository.findById((long) 15)).thenReturn(Optional.of(user));
		User user1 = userServiceImpl.getUserById(15);
		Assert.assertNotNull(user1);
		Assert.assertEquals("jansi@gmail.com", user1.getEmail());
	}
	

	@Test(expected = UserNotFoundException.class)
	public void testFindByIdForException() throws UserNotFoundException {
		User user = new User((long) 15, "jansi", "jansi@gmail.com", "jansi");
		Mockito.when(userRepository.findById((long) 15)).thenReturn(Optional.of(user));
		User user1 = userServiceImpl.getUserById(14);
		Assert.assertNotNull(user1);
		Assert.assertEquals("jansi@gmail.com", user1.getEmail());
	}


	@Test
	public void testFindByNameForNegative() {
		User user = new User((long) -1, "jansi", "jansi@gmail.com", "jansi");
		Mockito.when(userRepository.findByUsername("jansi")).thenReturn(user);
		User user1 = userServiceImpl.readByUserName("jansi");
		Assert.assertNotNull(user1);
		Assert.assertEquals("jansi", user1.getUsername());
	}

}
