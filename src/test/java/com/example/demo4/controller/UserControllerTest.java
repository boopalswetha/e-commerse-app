package com.example.demo4.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.demo4.dto.UserDTO;
import com.example.demo4.model.User;
import com.example.demo4.service.UserService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserControllerTest {

	@Autowired
	private UserController userController;

	@Autowired
	private UserService userService;

	@Test(expected = NullPointerException.class)
	public void testFindByNameForPositive() {
		@SuppressWarnings("unused")
		User user = new User((long) 1, "jansi", "jansi@gmail.com", "jansi");
		Mockito.when(userService.readByUserName("jansi")).thenReturn(null);
		UserDTO user1 = userController.getUserByName("jansi");
		Assert.assertNotNull(user1);
		Assert.assertEquals("jansi", user1.getUsername());
	}

	@Test(expected = NullPointerException.class)
	public void testFindByNameForNegative() {
		@SuppressWarnings("unused")
		User user = new User((long) -1, "rajkumar", "jansi@gmail.com", "jansi");
		Mockito.when(userService.readByUserName("rajkumar")).thenReturn(null);
		UserDTO user1 = userController.getUserByName("rajkumar");
		Assert.assertNotNull(user1);
		Assert.assertEquals("rajkumar", user1.getUsername());
	}

}
