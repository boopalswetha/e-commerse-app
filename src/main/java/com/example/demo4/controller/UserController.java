package com.example.demo4.controller;

import java.util.List;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo4.dto.ConverterService;
import com.example.demo4.dto.ResponseMessageDto;
import com.example.demo4.dto.UserDTO;
import com.example.demo4.dto.UserLoginDto;
import com.example.demo4.exception.UserNotFoundException;
import com.example.demo4.model.User;
import com.example.demo4.service.UserService;





@RestController

public class UserController {
	
	@Autowired
	private UserService userService;

	@Autowired
	private ConverterService converterService;
	

	/*@GetMapping(value = "/users")
	public ResponseEntity<List<User>> getAllUsers(User user) {
		List<User> users = userService.getAllUsers();
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	*/
	/*@GetMapping(value = "/user/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
		User user = userService.getUserById(id);
		return new ResponseEntity<User>(user, HttpStatus.OK);

	}*/
	
	/*
	 * 
	@GetMapping(value = "/user1/{another}")
	public ResponseEntity<List<User>> readProductByName(@PathVariable("another") String value) {
		List<User> users = userService.readByUserName(value);
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	 */
	

	
	@GetMapping(value = "/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO getUserById(@PathVariable("id") long id) {
       
        User user = userService.getUserById(id);
        return converterService.convertToDto(user);
    }
	

	@GetMapping(value = "/users")
	public List<UserDTO> getAllUsers(User user) {
		return userService.getAllUsers();
	}


	@GetMapping(value = "/user1/{another}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO getUserByName(@PathVariable("another") String value) {
       
        User user = userService.readByUserName(value);
        return converterService.convertToDto(user);
    }
	

	@PostMapping("/login")
	public ResponseEntity<ResponseMessageDto> login(@RequestBody UserLoginDto userLoginDto) throws UserNotFoundException {
		ResponseMessageDto responseMessageDto = new ResponseMessageDto();
		userService.login(userLoginDto);
		responseMessageDto.setMessage("user Logged in Sucessfully");
		return new ResponseEntity<>(responseMessageDto, HttpStatus.OK);

	}

}



