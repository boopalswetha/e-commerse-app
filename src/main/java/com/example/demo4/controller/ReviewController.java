package com.example.demo4.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo4.dto.ResponseDto;
import com.example.demo4.dto.ReviewRequestDto;
import com.example.demo4.exception.ReviewException;
import com.example.demo4.service.ReviewService;

@RestController
public class ReviewController {
	
	@Autowired
	ReviewService reviewService;
	

	@PostMapping("/review")
	public ResponseEntity<ResponseDto> review(@Valid @RequestBody ReviewRequestDto reviewRequestDto) throws ReviewException {
		return new ResponseEntity<>(reviewService.review(reviewRequestDto), HttpStatus.OK);
	}
}
