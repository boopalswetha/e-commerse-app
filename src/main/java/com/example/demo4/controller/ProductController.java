package com.example.demo4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo4.model.Category;
import com.example.demo4.model.Product;
import com.example.demo4.service.ProductService;


@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	

	@GetMapping(value = "/product")
	public ResponseEntity<List<Product>> getAllProducts(Product product) {
		List<Product> products = productService.getAllProducts();
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}
	

	@GetMapping(value = "/product/{product8}")
	public ResponseEntity<List<Product>> readProductByNameLike(@PathVariable("product8") String var1) {
		List<Product> product = productService.readByProductNameLike(var1 + "%");
		return new ResponseEntity<List<Product>>(product, HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public Category createCategory(@RequestBody Category category) {
		Category categorys = productService.createCategory(category);
		return categorys;
	}
	

}
