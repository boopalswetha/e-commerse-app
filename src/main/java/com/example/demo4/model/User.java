package com.example.demo4.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long UserId;
	private String username;
	private String email;
	private String password;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
	private List<Order> orders;

	public User() {
		super();
	}

	public User(Long userId, String username, String email, String password) {
		super();
		UserId = userId;
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public User(Long userId, String username, String email, String password, List<Order> orders) {
		super();
		UserId = userId;
		this.username = username;
		this.email = email;
		this.password = password;
		this.orders = orders;
	}

	public Long getUserId() {
		return UserId;
	}

	public void setUserId(Long userId) {
		UserId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		return "User [UserId=" + UserId + ", username=" + username + ", email=" + email + ", password=" + password
				+ ", orders=" + orders + "]";
	}

}
