package com.example.demo4.model;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "orders")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private float amount;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "order")
	private Set<Product> productSet;

	@ManyToOne
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private User user;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "trasnsction_id")
	@JsonIgnore
	private Trasnsction trasnsction;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Set<Product> getProductSet() {
		return productSet;
	}

	public void setProductSet(Set<Product> productSet) {
		this.productSet = productSet;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Trasnsction getTrasnsction() {
		return trasnsction;
	}

	public void setTrasnsction(Trasnsction trasnsction) {
		this.trasnsction = trasnsction;
	}

	public Collection<Double> getProductList() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object getAccountNumber() {
		// TODO Auto-generated method stub
		return null;
	}

}

