package com.example.demo4.exception;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.modelmapper.spi.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(UserNotFoundException.class)
	public final ResponseEntity<ErrorResponse> handleNotFoundException(UserNotFoundException ex, WebRequest request) {
		ErrorResponse errorResponse = new ErrorResponse(new Date(), ex.getMessage(), request.getDescription(false),
				HttpStatus.NOT_ACCEPTABLE.getReasonPhrase());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NOT_ACCEPTABLE);
	}

	@ExceptionHandler(ProductNotFoundException.class)
	public final ResponseEntity<ErrorResponse> handleNotFoundException(ProductNotFoundException ex,
			WebRequest request) {
		ErrorResponse errorResponse = new ErrorResponse(new Date(), ex.getMessage(), request.getDescription(false),
				HttpStatus.NOT_ACCEPTABLE.getReasonPhrase());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NOT_ACCEPTABLE);
	}

	@ExceptionHandler(AccountNotFound.class)
	public ResponseEntity<Object> AccountNotFound(AccountNotFound ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("TIMESTAmp ", LocalDateTime.now());
		body.put("message", "No Account data found with given account number");
		body.put("status", HttpStatus.NOT_FOUND);

		body.put("stackTrace", ex.getClass());
		return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(BanificiaryNotFoundException.class)
	public ResponseEntity<Object> DataNotFoundException(BanificiaryNotFoundException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", "No Banificiarydata found with given account number");

		return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(InSufficientBalanceInAccount.class)
	public ResponseEntity<Object> InSufficientBalanceInAccount(InSufficientBalanceInAccount ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("TIMESTAmp ", LocalDateTime.now());
		body.put("message", "no sufficient balance in the Account Plese check the balance and try again");
		body.put("status", HttpStatus.BAD_REQUEST);

		body.put("stackTrace", ex.getClass());
		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(TransactionFailedException.class)
	public ResponseEntity<Object> InSufficientBalanceInAccount(TransactionFailedException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("TIMESTAmp ", LocalDateTime.now());
		body.put("message", "Transaction is failed");
		body.put("status", HttpStatus.BAD_REQUEST);

		body.put("stackTrace", ex.getClass());
		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ReviewException.class)
	public ResponseEntity<Object> ReviewException(ReviewException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", LocalDateTime.now());
		body.put("message", "this product is not found");

		return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	}

}
