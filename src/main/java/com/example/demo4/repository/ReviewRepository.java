package com.example.demo4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo4.model.Product;
import com.example.demo4.model.Review;

public interface ReviewRepository extends JpaRepository<Review, Integer>{

}
