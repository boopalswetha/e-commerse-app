package com.example.demo4.dto;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.example.demo4.model.Product;
import com.example.demo4.model.User;



public class OrderDtoResponse {
	private int orderId;
	private int accountNumber;
	
	private double totalOrderAmmount;
	private User user;

	public double getTotalOrderAmmount() {
		return totalOrderAmmount;
	}
	public void setTotalOrderAmmount(double totalOrderAmmount) {
		this.totalOrderAmmount = totalOrderAmmount;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Product> productList;
	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public List<Product> getProductList() {
		return productList;
	}
	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
	
	
	
}

