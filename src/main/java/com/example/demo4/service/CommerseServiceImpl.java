package com.example.demo4.service;

import java.util.List;
import java.util.stream.Collectors;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo4.dto.OrderDtoResponse;
import com.example.demo4.exception.TransactionFailedException;
import com.example.demo4.model.Order;
import com.example.demo4.repository.OrderRepo;
import com.example.demo4.repository.UserRepository;



@Service
public class CommerseServiceImpl implements CommerseService {

	@Autowired
	OrderRepo orderRepo;
	@Autowired
	UserRepository userRepository;
	@Autowired
	RestTemplate restTemplate;

	@Bean
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Override
	public OrderDtoResponse buyProject(Order order) {

		double cash = this.sum(order);
		order.setUser(userRepository.findById(order.getUser().getUserId()).get());

		ResponseEntity<String> response = this.transction(order, cash);
		if (response.getStatusCode() == HttpStatus.OK) {

			order = orderRepo.save(order);
			OrderDtoResponse orderDtoResponse = new OrderDtoResponse();
			BeanUtils.copyProperties(order, orderDtoResponse);
			orderDtoResponse.setAccountNumber((int) order.getAccountNumber());
			orderDtoResponse.setTotalOrderAmmount(cash);
			orderDtoResponse.setOrderId(order.getId());
			return orderDtoResponse;

		} else {
			throw new TransactionFailedException();
		}

	}

	double sum(Order order) {
		// TODO Auto-generated method stub
		return 0;
	}

	public ResponseEntity<String> transction(Order order, double sum) {
		String uri = "http://localhost:8081/transaction";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();
		request.put("accountNumber", order.getAccountNumber());
		request.put("amount", sum);
		request.put("banificiaryAccountNo", 6666);

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		System.out.println(response);
		return response;
	}

	/*public double sum(Order order) {

		List<Double> d = order.getProductList().stream().map(x -> x.getPrice()).collect(Collectors.toList());
		double sum = d.stream().mapToDouble(Double::doubleValue).sum();
		return sum;

	}*/



}

