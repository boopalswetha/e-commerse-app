package com.example.demo4.service;

import java.util.List;

import com.example.demo4.model.Order;


public interface OrderService {
	
	public List<Order> getAllOrders();
	  public Order getOrderById(int orderId);

}
