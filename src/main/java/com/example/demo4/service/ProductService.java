package com.example.demo4.service;

import java.util.List;

import com.example.demo4.model.Category;
import com.example.demo4.model.Product;





public interface ProductService {
	
	public List<Product> getAllProducts();
	public List<Product> readByProductNameLike(String name);
	public Category createCategory(Category category);

}
