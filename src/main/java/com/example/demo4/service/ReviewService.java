package com.example.demo4.service;

import javax.validation.Valid;

import com.example.demo4.dto.ResponseDto;
import com.example.demo4.dto.ReviewRequestDto;
import com.example.demo4.exception.ReviewException;

public interface ReviewService {

	public ResponseDto review(@Valid ReviewRequestDto reviewRequestDto) throws ReviewException;

	

}
