package com.example.demo4.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo4.exception.ProductNotFoundException;
import com.example.demo4.model.Category;
import com.example.demo4.model.Product;
import com.example.demo4.repository.ProductRepository;
import com.example.demo4.repository.ppRepository;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	ppRepository pprepository;

	@Override
	public List<Product> getAllProducts() {
		List<Product> productList = (List<Product>) productRepository.findAll();

		if (productList.size() > 0) {
			return productList;
		} else {
			return new ArrayList<Product>();
		}

	}

	@Override
	public List<Product> readByProductNameLike(String name) throws ProductNotFoundException {
		List<Product> product = productRepository.findByProductNameLike(name);
		if (product != null) {
			return product;
		} else {
			throw new ProductNotFoundException("product with name" + name + "is not found");
		}
	}

	@Override
	public Category createCategory(Category category) {
		// TODO Auto-generated method stub
		return pprepository.save(category);
	}

}
