package com.example.demo4.service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo4.dto.ConverterService;
import com.example.demo4.dto.UserDTO;
import com.example.demo4.dto.UserLoginDto;
import com.example.demo4.exception.UserNotFoundException;
import com.example.demo4.model.User;
import com.example.demo4.repository.UserRepository;



@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ConverterService converterService;

	/*@Override
	public List<User> getAllUsers() {
		List<User> userList = (List<User>) userRepository.findAll();

		if (userList.size() > 0) {
			return userList;
		} else {
			return new ArrayList<User>();
		}
	}*/

	@Override
	public User getUserById(long userId) {
		Optional<User> option = userRepository.findById(userId);
		User user = null;
		if (option.isPresent()) {
			user = option.get();

		} else {
			throw new UserNotFoundException("User with id: "+userId+"  Not found");
		}
		return user;
	}

	@Override
	public User readByUserName(String name) {
		
		return this.userRepository.findByUsername(name);
	}

	@Override
	public List<UserDTO> getAllUsers() {
		List<User> userList = (List<User>) userRepository.findAll();

		return userList.stream().map(converterService::convertToDto).collect(Collectors.toList());
	}
	

	@Override
	public User login(UserLoginDto userLoginDto) throws UserNotFoundException {
		UserLoginDto dto = new UserLoginDto();
		User userLogin = new User();

		BeanUtils.copyProperties(userLoginDto, userLogin);

		User login = userRepository.findByUsernameAndPassword(userLogin.getUsername(), userLogin.getPassword());
		if (login == null) {
			throw new UserNotFoundException("Enter correct Id and Password");
		}
		return login;
		
		

	}

}

	
	
	


